#ifndef MINISLAB_H
#define MINISLAB_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _minislab_t minislab_t;

minislab_t *minislab_new     (size_t      element_size);
void       *minislab_alloc   (minislab_t *self);
void        minislab_release (minislab_t *self,
                              void       *ptr);
void        minislab_free    (minislab_t *self);

#ifdef __cplusplus
}
#endif

#endif /* MINISLAB_H */
