# minislab

This is a stupid naive attempt at making a buffer pool for structures of the same size.
It's not fancy like GSlice (which is slower than malloc and worse on memory size on GNUlib these days anyway).
It also requires that allocations are less than half a page.
It's also doing stupid stuff like checking bitvectors a bit at a time.

In general, it sucks and you shouldn't use it.
