all: minislab-test

FILES = minislab-test.c minislab.c minislab.h
WARNINGS = -Wall -Werror
DEBUG = 
OPTS = -O2

minislab-test: $(FILES)
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTS) $(FILES)

clean:
	rm -f minislab-test
