#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "minislab.h"

#define N_ITERATIONS 100000

int
main (int argc,
      char *argv[])
{
  void **data;
  minislab_t *slab;
  size_t i;

  data = malloc (sizeof (void*) * N_ITERATIONS);
  assert (data != NULL);

  slab = minislab_new (128);
  assert (slab != NULL);

  for (i = 0; i < N_ITERATIONS; i++)
    {
      data [i] = minislab_alloc (slab);
      if (data [i] == NULL)
        printf ("%llu\n", (long long unsigned)i);
      assert (data [i] != NULL);
    }

  for (i = 0; i < N_ITERATIONS; i++)
    {
      minislab_release (slab, data [i]);
    }

  minislab_free (slab);

  return 0;
}
