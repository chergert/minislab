#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/user.h>

#include "minislab.h"

#if 0
# define ASSERT assert
# define ASSERT_NOT_REACHED() assert(0)
#else
# define ASSERT(a)
# define ASSERT_NOT_REACHED()
#endif

#define MINISLAB_MALLOC malloc
#define MINISLAB_MALLOC0(size) calloc(1,size)
#define MINISLAB_REALLOC(ptr,size) realloc(ptr,size)
#define MINISLAB_FREE free
#define MINISLAB_PAGE_SIZE 4096
#define MINISLAB_PAGE_DATA(self,page) (((uint8_t *)page)+self->base_offset)

static inline bool
is_power_of_two (size_t x)
{
  return ((x != 0) && !(x & (x - 1)));
}

static inline size_t
next_power_of_two (size_t v)
{
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
#if __WORDSIZE == 64
  v |= v >> 32;
#endif
  v++;

  return v;
}


/*
 * NOTE:
 *
 * This is really slow, and naive.
 * But served as a fun hour hack.
 */

typedef struct
{
  uint8_t bitvec[128];
  uint8_t data[0];
} minislab_page_t;

struct _minislab_t
{
  pthread_mutex_t    lock;
  uint16_t           element_size;
  uint16_t           n_elements;
  uint16_t           base_offset;
  size_t             n_pages;
  minislab_page_t  **pages;
  minislab_page_t   *last_page;
};

static inline void
BitArray_Set (void   *buf,
              size_t  bit,
              bool    value)
{
  if (value) {
    ((uint8_t *)buf) [bit >> 3] |= (1U << (bit & 0x7));
  } else {
    ((uint8_t *)buf) [bit >> 3] &= ~(1U << (bit & 0x7));
  }
}

static inline bool
BitArray_Get (void   *buf,
              size_t  bit)
{
  return ((((uint8_t *)buf) [bit >> 3] >> (bit & 0x7)) & 0x1);
}


static inline bool
minislab_page_contains (const minislab_t      *self,
                        const minislab_page_t *page,
                        const void            *element)
{
  return (((const void *)element > (const void *)page) &&
          ((const uint8_t *)element < (const uint8_t *)page+MINISLAB_PAGE_SIZE));
}

/**
 * minislab_new:
 * @element_size: the size of elements to allocate when a new allocation
 *   is requested. This must be within 32 and 2048.
 *
 * Allocates a new minislab that will hand out allocations of size
 * @element_size. This works by keeping an array of active pages handy and a
 * small free list at the beginning of the page.
 *
 * PAGE_SIZE is assumed to be 4096.
 *
 * Returns: a #minislab_t or %NULL upon failure and errno is set.
 */
minislab_t *
minislab_new (size_t element_size)
{
  minislab_t *self;

  if (!is_power_of_two (element_size))
    element_size = next_power_of_two (element_size);

  if ((element_size > 2048) || (element_size < 32))
    {
      errno = EINVAL;
      return NULL;
    }

  self = MINISLAB_MALLOC (sizeof *self);
  if (self == NULL)
    goto failure;

  self->element_size = element_size;
  self->base_offset = (self->element_size > 128) ? self->element_size : 128;
  self->n_elements = (MINISLAB_PAGE_SIZE-self->base_offset) / self->element_size;
  self->n_pages = 1;
  self->pages = MINISLAB_MALLOC (sizeof (void *));
  if (self->pages == NULL)
    goto failure;

  self->pages [0] = MINISLAB_MALLOC0 (MINISLAB_PAGE_SIZE);
  if (self->pages [0] == NULL)
    goto failure;

  pthread_mutex_init (&self->lock, NULL);

  errno = 0;

  return self;

failure:
  if (self != NULL)
    {
      if (self->pages != NULL)
        {
          if (self->pages [0] != NULL)
            MINISLAB_FREE (self->pages [0]);
          MINISLAB_FREE (self->pages);
        }
    }

  MINISLAB_FREE (self);

  errno = ENOMEM;

  return NULL;
}

static void
minislab_reclaim (minislab_t      *self,
                  minislab_page_t *page,
                  size_t           idx)
{
  ASSERT (self);
  ASSERT (page);
  ASSERT (self->pages [idx] == page);

  if (self->n_pages > 1)
    {
      if (idx == (self->n_pages - 1))
        {
          self->n_pages--;
          self->pages [idx] = NULL;
          MINISLAB_FREE (page);
        }
      else
        {
          self->n_pages--;
          memmove (&self->pages [idx], &self->pages [idx+1], (self->n_pages-idx) * sizeof(void*));
          MINISLAB_FREE (page);
        }
    }
  else
    {
      /* never free the last page to avoid flapping */
    }
}

static bool
minislab_maybe_reclaim (minislab_t      *self,
                        minislab_page_t *page,
                        size_t           idx)
{
  static const uint8_t empty[128] = { 0 };

  ASSERT (self);
  ASSERT (page);
  ASSERT (self->pages [idx] == page);

  if (memcmp (page->bitvec, empty, sizeof empty) == 0)
    {
      if (page == self->last_page)
        self->last_page = NULL;
      minislab_reclaim (self, page, idx);
      return true;
    }

  return false;
}

static int
find_element_page (const void *keyptr,
                   const void *arptr)
{
  const void *page = *(const void **)arptr;

  if (keyptr < page)
    return -1;
  else if (keyptr >= (const void *)(((uint8_t *)page)+MINISLAB_PAGE_SIZE))
    return 1;
  return 0;
}

void
minislab_release (minislab_t *self,
                  void       *element)
{
  minislab_page_t **ret;
  minislab_page_t *page;
  size_t offset;
  size_t page_num;
  unsigned idx;

  ASSERT (self != NULL);

  /* Be like free(), allowing NULL */
  if (element == NULL)
    return;

  pthread_mutex_lock (&self->lock);

  ret = bsearch (element, self->pages, self->n_pages, sizeof(void*), find_element_page);

  ASSERT (ret != NULL);

  page = *ret;
  page_num = (ret - self->pages);
  ASSERT (self->pages [page_num] == page);

  ASSERT (minislab_page_contains (self, page, element));

  offset = ((const uint8_t *)element) - (((const uint8_t *)page) + sizeof(page->bitvec));

  if (offset % self->element_size != 0)
    {
      ASSERT_NOT_REACHED ();
    }

  idx = (offset / self->element_size);
  BitArray_Set (page->bitvec, idx, false);

  minislab_maybe_reclaim (self, page, page_num);

  pthread_mutex_unlock (&self->lock);
}

static int
compare_ptr (const void *aptr,
             const void *bptr)
{
  const void *a = *(const void **)aptr;
  const void *b = *(const void **)bptr;

  if (a > b)
    return 1;
  else if (a < b)
    return -1;
  else
    return 0;
}

void *
minislab_alloc (minislab_t *self)
{
  minislab_page_t *new_page;
  void *ret = NULL;
  size_t i;

  ASSERT (self);

  pthread_mutex_lock (&self->lock);

  if (self->last_page != NULL)
    {
      minislab_page_t *page = self->last_page;
      size_t j;

      for (j = 0; j < self->n_elements; j++)
        {
          if (BitArray_Get (page->bitvec, j) == 0)
            {
              BitArray_Set (page->bitvec, j, true);
              ret = MINISLAB_PAGE_DATA (self, page) + (j * self->element_size);
              goto unlock;
            }
        }
    }

  for (i = 0; i < self->n_pages; i++)
    {
      minislab_page_t *page = self->pages [i];
      size_t j;

      for (j = 0; j < self->n_elements; j++)
        {
          if (BitArray_Get (page->bitvec, j) == 0)
            {
              BitArray_Set (page->bitvec, j, true);
              ret = MINISLAB_PAGE_DATA (self, page) + (j * self->element_size);
              goto unlock;
            }
        }
    }

  new_page = MINISLAB_MALLOC0 (MINISLAB_PAGE_SIZE);

  if (new_page == NULL)
    {
      errno = ENOMEM;
      goto unlock;
    }

  self->pages = MINISLAB_REALLOC (self->pages, (self->n_pages+1) * MINISLAB_PAGE_SIZE);

  if (self->pages == NULL)
    {
      errno = ENOMEM;
      goto unlock;
    }

  self->pages [self->n_pages++] = new_page;
  BitArray_Set (new_page->bitvec, 0, true);
  self->last_page = new_page;

  qsort (self->pages, self->n_pages, sizeof(void*), compare_ptr);

  ret = MINISLAB_PAGE_DATA (self, new_page);

unlock:
  pthread_mutex_unlock (&self->lock);

  return ret;
}

void
minislab_free (minislab_t *self)
{
  size_t i;

  ASSERT (self);

  pthread_mutex_destroy (&self->lock);
  for (i = 0; i < self->n_pages; i++)
    MINISLAB_FREE (self->pages [i]);
  MINISLAB_FREE (self->pages);
  MINISLAB_FREE (self);
}
